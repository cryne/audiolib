import { buildSchema } from "graphql";

export default buildSchema(`
    type Query {
        message: String,
        songs: [
            Song
        ],
    }
    type Song {
        name: String,
        location: String,
        metadata : Metadata
    }
    type Metadata {
        duration: Int,
        filetype: String
    }
`);
