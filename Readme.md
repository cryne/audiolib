# Audio Library

## About 
This project is built with node.js, express, graphql, and websockets. It's
a stub intended to be built up in future interations.

## Setup

Everything you need is included in the repo. 
Run npm install and Use npm start to run the server.
Navigate to localhost:4000/api/ql to access the graphql explorer. Use this
query to give it a test
```
{
    songs {
        name
        location
        metadata {
            duration
            filetype
        }
    }
}
```

I've made a public s3 bucket that you should be able to use, however if you
want your own, change the bucket name in the Config.js and use your own
bucket. Be sure to add the sample.mp3 file to it. If you need to create
a bucket, and you have aws cli installed, you can use createS3Bucket that I've
included in the repo.

## What next?

Right now the metadata is being loaded from a file, but I think the best
approach would be to have files dropped into s3, and then file off an event,
which triggers a lambda function, that gathers the files metadata and stores it
in either a file or a db like dynamo. I could server the metadata over a rest
api and funnel that into graphql (I could abandon node altogether in favor of
a complete serverless setup too.).

The metadata is just duration and filetype. This is because the project is
a stub and I didn't want to get to heavy on details. I could update this.

I haven't included a means to upload files, only serve them. I could expand on
that as well.
