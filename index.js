import { SubscriptionServer } from "subscriptions-transport-ws";
import { execute, subscribe } from "graphql";
import AWS from "aws-sdk";
import GraphHTTP from "express-graphql";
import express from "express";

import { Server } from "http";

import config from "./Config";
import metadata from "./metadata.json";
import schema from "./schema";

/** BASE Express server definition **/
const app = express();

AWS.config.update({
  region: config.region
});
const s3 = new AWS.S3();

const bucketParams = {
  Bucket: config.Bucket
};

const root = {
  message: async () => {
    return "Hello World";
  },
  songs: async () => {
    const request = s3.listObjectsV2(bucketParams).promise();

    return request
      .then(data => {
        const bucket = data.Name;

        return data.Contents.map(file => {
          return {
            name: file.Key,
            location: `https://${bucket}.s3.amazonaws.com/${file.Key}`,
            metadata: metadata[file.Key]
          };
        });
      })
      .catch(err => {
        console.log(err);
      });
  }
};

// main endpoint for GraphQL Express
app.use(
  "/api/ql",
  GraphHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true
  })
);

// Making plain HTTP server for Websocket usage
const server = Server(app);

/** GraphQL Websocket definition **/
SubscriptionServer.create(
  {
    schema,
    rootValue: root,
    execute,
    subscribe
  },
  {
    server: server,
    path: "/api/ws"
  }
);

server.listen(4000, () => {
  console.log("Server started here -> http://0.0.0.0:4000");
});
